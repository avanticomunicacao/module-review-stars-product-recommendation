<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Avanti\ReviewStarsProdRecommendations\Controller\Index;

use Magento\Catalog\Block\Product\ReviewRendererInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use function GuzzleHttp\Psr7\str;


class Index extends Action
{
    const TEMPLATE_TYPE = 'short';

    protected $resultPageFactory;
    protected $json;
    protected $reviewRenderer;
    protected $productRepository;

    /**
     * Constructor
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Json $json
     * @param LoggerInterface $logger
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Json $json,
        LoggerInterface $logger,
        \Magento\Catalog\Block\Product\Context $reviewRenderer,
        ProductRepositoryInterface $productRepository
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->json = $json;
        $this->logger = $logger;
        $this->reviewRenderer = $reviewRenderer->getReviewRenderer();
        $this->productRepository = $productRepository;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        try {
            $productId = $this->getRequest()->getParam('productId');
            if (!$productId) {
                throw new LocalizedException(__("Product Id not Informed"));
            }

            $product = $this->productRepository->getById($productId);

            $reviews = $this->getReviewsSummaryHtml($product, self::TEMPLATE_TYPE, true);
            $newStr = str_replace(array("\n", "            ", "        ","        ","    "), "", $reviews);
            return $this->jsonResponse($newStr);

        } catch (LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->json->serialize($response)
        );
    }

    /**
     * Get product reviews summary
     *
     * @param Product $product
     * @param bool $templateType
     * @param bool $displayIfNoReviews
     * @return string
     */
    public function getReviewsSummaryHtml(
        Product $product,
        $templateType = false,
        $displayIfNoReviews = false
    ) {
        return $this->reviewRenderer->getReviewsSummaryHtml($product, $templateType, $displayIfNoReviews);
    }

}
